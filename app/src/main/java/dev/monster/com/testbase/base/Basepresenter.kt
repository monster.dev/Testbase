package dev.monster.com.testbase.base

/**
 * Created by duc on 5/1/2018.
 */
interface Basepresenter<V> {

    fun attachView(view: V)

    fun detachView()

}