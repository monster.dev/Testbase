package dev.monster.com.testbase

import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dev.monster.com.testbase.injection.component.ActivityComponent
import dev.monster.com.testbase.injection.module.Activitymodule
import dev.monster.com.testbase.injection.module.NetworkModule

/**
 * Created by duc on 5/2/2018.
 */
class MyApplication : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder()
                .application(this)
                .build()
    }


}