package dev.monster.com.testbase.network

import dev.monster.com.testbase.models.CityListResponse
import io.reactivex.Observable
import io.reactivex.Observer
import retrofit2.http.POST
import retrofit2.http.GET


/**
 * Created by duc on 5/1/2018.
 */
interface NetworkService {
    @GET("v1/city")
    fun getCityList(): Observable<CityListResponse>
}