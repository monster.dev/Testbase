package dev.monster.com.testbase.models

/**
 * Created by duc on 5/1/2018.
 */
class CityListResponse(val data: CityListData,
                       val message: String,
                       val status: Int) {

}