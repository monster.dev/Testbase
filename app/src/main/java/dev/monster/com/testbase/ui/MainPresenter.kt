package dev.monster.com.testbase.ui

import dev.monster.com.testbase.models.CityListResponse
import dev.monster.com.testbase.network.NetworkService
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by duc on 5/1/2018.
 */
class MainPresenter : MainContract.MainPresenter {
    @Inject
    lateinit var networkService: NetworkService
    var mView: MainContract.View? = null
    var mDisposable: Disposable? = null

    override fun getData() {
        networkService.getCityList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeWith(object : Observer<CityListResponse> {
                    override fun onError(e: Throwable) {

                    }

                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onNext(t: CityListResponse) {
                        mView?.showData(t)
                    }

                })
    }

    override fun attachView(view: MainContract.View) {
        mView = view
    }

    override fun detachView() {
        mView = null
        if (mDisposable != null) mDisposable!!.dispose()
    }

}