package dev.monster.com.testbase.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import dagger.android.support.DaggerAppCompatActivity
import dev.monster.com.testbase.injection.component.ActivityComponent
import dev.monster.com.testbase.injection.module.Activitymodule
import dev.monster.com.testbase.injection.module.NetworkModule

/**
 * Created by duc on 5/1/2018.
 */
abstract class BaseActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getViewId())
        onViewReady()
    }
    abstract fun getViewId(): Int

    abstract fun onViewReady()


}